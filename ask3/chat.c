#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>
#include <limits.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/select.h>

#include <crypto/cryptodev.h>
#define DATA_SIZE       256
#define BLOCK_SIZE      16
#define KEY_SIZE        16  /* AES128 */

#include <arpa/inet.h>
#include <netinet/in.h>
#define MSG_MAX_SIZE 1024
#define TCP_BACKLOG 5

unsigned char iv[BLOCK_SIZE] = "rmi+HuMPrgj8YDZa";
unsigned char password[KEY_SIZE];

/*
interface:

a.out connect host port [password]
a.out listen port [password] 
*/

unsigned char buf_encr[256], buf_plain[256];
int use_encryption;

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt) {
	ssize_t ret;
	size_t orig_cnt = cnt;

	while (cnt > 0) {
		ret = write(fd, buf, cnt);
		if (ret < 0)
			return ret;
		buf += ret;
		cnt -= ret;
	}

	return orig_cnt;
}

void my_error(const char *s) {
	if(errno != 0)
		perror(s);
	else
		fprintf(stderr,"%s\n",s);
	exit(EXIT_FAILURE);
}

void usage(char **argv) {
	fprintf(stderr,"%s connect host port [password]\n"
	"%s listen port [password]\n",*argv,*argv);
}

long read_long(char *str) {
	char *endptr;
	errno = 0;    /* To distinguish success/failure after call */

	long val = strtol(str, &endptr, 10);

	/* Check for various possible errors */

	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
	|| (errno != 0 && val == 0)) {
		perror("ERROR: strtol");
		exit(EXIT_FAILURE);
	}
	if (endptr == str) {
		fprintf(stderr, "No digits were found\n");
		exit(EXIT_FAILURE);
	}
	return val;
}

struct session_op sess;
struct crypt_op cryp;
int cfd;

int init_crypto () {
	cfd = open("/dev/crypto", O_RDWR);
	if (cfd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}

	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));

	/*
	 * Get crypto session for AES128
	 */
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = password;

	cryp.len = DATA_SIZE;
	cryp.iv = iv;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}
	
	cryp.ses = sess.ses;

	return 0;
}

int encrypt (unsigned char *plaintext, unsigned char *encrypted) {
	cryp.src = plaintext;
	cryp.dst = encrypted;
	cryp.op = COP_ENCRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		return 1;
	}
	return 0;
}

int decrypt (unsigned char *encrypted, unsigned char *plaintext) {
	cryp.src = encrypted;
	cryp.dst = plaintext;
	cryp.op = COP_DECRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		return 1;
	}
	return 0;
}

int sd;
ssize_t n;

void read_and_send()
{
	int size = 0;
	unsigned char* buf = use_encryption ? buf_encr : buf_plain;
	if (ioctl(0, FIONREAD, &size) == 0 && size > 0){
		memset(buf_plain, 0, DATA_SIZE);
		memset(buf_encr, 0, DATA_SIZE);
		while(size > 0 && (n = read(0,buf_plain,sizeof(buf_plain))) > 0){ // should we 0 terminate?????
			if (use_encryption) {
				//size = DATA_SIZE;
				n = DATA_SIZE;
				if (encrypt(buf_plain, buf_encr) > 0) {
					perror("unable to encrypt");
					exit(1);
				}
			}
			if (insist_write(sd, buf, n) != n) {
				perror("write");
				exit(1);
			}
			size -= n;
		}
		if (n < 0) {
			perror("read");
			exit(1);
		}
	}
}

void receive (int newsd) {
	int size = 0;
	unsigned char* buf = use_encryption ? buf_encr : buf_plain; 
	
	if (ioctl(newsd, FIONREAD, &size) == 0 && size > 0){
		memset(buf_plain, 0, DATA_SIZE);
		memset(buf_encr, 0, DATA_SIZE);
		putchar('>');
		fflush(stdout);
		while(size > 0){
			n = read(newsd, buf, sizeof(buf_plain));
			if (use_encryption) {
				//size = DATA_SIZE;
				n = DATA_SIZE;
				if (decrypt(buf_encr, buf_plain) > 0) {
					perror("unable to decrypt");
					exit(1);
				}
			}
				
			if (n <= 0) {
				if (n < 0)
					perror("read from remote peer failed");
				else
					fprintf(stderr, "Peer went away\n");
				break;
			}
			insist_write(1,buf_plain,n);
			size -= n;
		}
		fflush(stdout);
	}	
}


/* Insist until all of the data has been read */
ssize_t insist_read(int fd, void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;

	while (cnt > 0) {
		ret = read(fd, buf, cnt);
		if (ret < 0)
			return ret;
		buf += ret;
		cnt -= ret;
	}

	return orig_cnt;
}

static int fill_urandom_buf(unsigned char *buf, size_t cnt) {
	int crypto_fd;
	int ret = -1;

	crypto_fd = open("/dev/urandom", O_RDONLY);
	if (crypto_fd < 0)
			return crypto_fd;

	ret = insist_read(crypto_fd, buf, cnt);
	close(crypto_fd);

	return ret;
}

void server(int port) {
	char addrstr[INET_ADDRSTRLEN];
	int newsd;
	socklen_t len;
	struct sockaddr_in sa;

	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Bind to a well-known port */
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port); //χαζομαρα interface
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", port);

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) {
		perror("listen");
		exit(1);
	}
	fd_set rfds;
	
	/* Loop forever, accept()ing connections */
	for (;;) {
		fprintf(stderr, "Waiting for an incoming connection...\n");

		/* Accept an incoming connection */
		len = sizeof(struct sockaddr_in);
		if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
			perror("accept");
			exit(1);
		}
		if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
			perror("could not format IP address");
			exit(1);
		}
		fprintf(stderr, "Incoming connection from %s:%d\n",
			addrstr, ntohs(sa.sin_port));
		sd = newsd;
		/* We break out of the loop when the remote peer goes away */
		for (;;) {
			FD_ZERO(&rfds);
			FD_SET(0,&rfds);
			FD_SET(newsd,&rfds);
			int ret = select(newsd+1,&rfds,NULL,NULL,NULL);
			if(FD_ISSET(0,&rfds))
				read_and_send();
			else if(FD_ISSET(newsd,&rfds))
				receive(newsd);
		}
		/* Make sure we don't leak open files */
		if (close(newsd) < 0)
			perror("close");

	}

	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
		perror("ioctl(CIOCFSESSION)");
	}
}

void client (char *hostname, int port) {
	ssize_t n;
	struct hostent *hp;
	struct sockaddr_in sa;

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Look up remote hostname on DNS */
	if ( !(hp = gethostbyname(hostname))) {
		printf("DNS lookup failed for host %s\n", hostname);
		exit(1);
	}

	/* Connect to remote TCP port */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
	fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
	if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		perror("connect");
		exit(1);
	}
	fprintf(stderr, "Connected.\n");
	fd_set rfds;

	for(;;){
		FD_ZERO(&rfds);
		FD_SET(0,&rfds);
		FD_SET(sd,&rfds);
		int ret = select(sd+1,&rfds,NULL,NULL,NULL);
		if(FD_ISSET(0,&rfds))
			read_and_send();
		else if(FD_ISSET(sd,&rfds))
			receive(sd);
		/*
		 * Let the remote know we're not going to write anything else.
		 * Try removing the shutdown() call and see what happens.
		 */
		/*if (shutdown(sd, SHUT_WR) < 0) {
			perror("shutdown");
			exit(1);
		}*/
	}

	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
		perror("ioctl(CIOCFSESSION)");
	}
}

int main(int argc, char **argv)
{
	char msg[MSG_MAX_SIZE];
	char host[256];
	int port;

	if(argc < 3){
		usage(argv);
		exit(EXIT_FAILURE);
	}if(strcmp(argv[1],"connect") == 0){
		if(argc < 4 || argc > 5){
			usage(argv);
			exit(EXIT_FAILURE);
		}
		strncpy(host,argv[2],256);
		host[255] = 0;
		port = read_long(argv[3]);
		if(argc == 5){
			use_encryption = 1;
			strncpy(password, argv[4], KEY_SIZE);
			
			if (init_crypto() > 0) {
				perror("init_crypto");
				exit(1);
			}
		}
		client(host,port);
	}else if(strcmp(argv[1],"listen") == 0){
		if(argc > 4){
			usage(argv);
			exit(EXIT_FAILURE);
		}
		port = read_long(argv[2]);
		if(argc == 4){
			use_encryption = 1;
			strncpy(password, argv[3], KEY_SIZE);
			if (init_crypto() > 0) {
				perror("init crypto");
				exit(1);
			}
		}
		server(port);
	}else{
		usage(argv);
		exit(EXIT_FAILURE);
	}
}
