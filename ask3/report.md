---
mainfont: DejaVu Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "Εργαστήριο λειτουργικών συστημάτων, 3η Σειρά ασκήσεων"
author:
- "Αγιάννης Κωνσταντίνος 03116352"
- "Καπελώνης Ελευθέριος 03116163"
---

# 1o και 2ο Ζητούμενο

Στο πρώτο μέρος της εργαστηριακής άσκησης ζητείται η δημιουργία μίας
εφαρμογής συνομιλίας με χρήση των BSD sockets. Τα δεδομένα θα αποστέλλονται
στο μέρος 1 ακρυπτογράφητο μέσα από το δίκτυο ενώ στο 2ο μέρος κρυπτογραφημένα.
Η διεπαφή γραμμής εντολών υλοποιείται με τις ακόλουθες εντολές:

```
./chat connect host port [password]
./chat listen port [password]
```

Ο εξυπηρετητής τρέχει την εντολή `./chat listen port` και ξεκινάει να ακούει
στην πόρτα που του έχει δοθεί. Ο πελάτης τρέχει την εντολή
`./chat connect host port` όπου host η διεύθυνση του εξυπηρετητή και port
η πόρτα στην οποία ακούει.

Ο εξυπηρετητής δημιουργεί μία υποδοχή (socket) με την εντολή socket,
και με χρήση της εντολής bind θέτει σε ποια πόρτα να ακούει.
Έπειτα με την εντολή listen ξεκινά να περιμένει για τους πελάτες.
Μετά μπαίνει σε έναν ατέρμονα βρόγχο όπου με την εντολή accept αποδέχεται
έναν πελάτη. Ύστερα με χρήση της εντολής select περιμένει μέχρι είτε
να έρθουν μηνύματα από τον πελάτη, είτε ο εξυπηρετητής έχει πατήσει κάτι
στο πληκτρολόγιο (πρότυπη είσοδος - stdin). Μετά ανάλογα με το από που ήρθε
είτε γράφει στο socket αν η είσοδος ήταν η πρότυπη είσοδος είτε γράφει στην πρότυπη έξοδο
αν ήρθαν δεδομένα από το socket.

Ο πελάτης παρόμοια με την εντολή socket φτιάχνει μία υποδοχή (socket).
Συνδέεται στον εξυπηρετητή με την εντολή connect και μετά κάνει την ίδια
χρήση της κλήσης select με τον εξυπηρετητή.


Για το κρυπτογραφημένο κομμάτι της εργασίας, έγινε χρήση της συσκευής
cryptodev που παρέχει μία διεπαφή για χρήση των επιταχυντών κρυπτογραφικών
συναρτήσεων του επεξεργαστή. Για τη χρήση της, αρχικά γίνεται open
της συσκευής, μετά καλείται μία ioctl έτσι ώστε να περαστούν ο αλγόριθμος
κρυπτογράφησης και ο κωδικός. Για κάθε χρήση της συσκευής για κρυπτογράφηση
και αποκρυπτογράφηση γίνεται μία κλήση της ioctl στην οποία δίνονται δείκτες
στα δεδομένα εισόδου και εξόδου.


# Ζητούμενο 3

Ο σκοπός είναι να εκμεταλλευτούμε την δυνατότητα paravirtualization έτσι ώστε να επιταχύνουμε τις κλήσεις στη συσκευή cryptodev από εικονικές μηχανές.
Χρησιμοποιήθηκε το πρότυπο VirtIO δίνοντας τη δυνατότητα ο guest να έχει το ίδιο API προς το cryptodev αλλά αυτές οι κλήσεις να γίνονται πιο αποδοτικά μέσω εικονικής συσκευής.

O driver του VirtIO έχει δύο μέρη: το frontend (στον guest) και το backend (στον host).

## Frontend driver

Προσθέσαμε ένα **σημαφόρο** που προστατεύει το `struct crypto_device` (αρχείο `crypto.h`). 
Στη συγκεκριμένη περίπτωση μπορούμε να τον χρησιμοποιήσουμε γιατί ενημερώνουμε τα πεδία του `crypto_device` μόνο σε process context, όταν δηλαδή κάποια εφαρμογή κάνεις κλήση προς τη συσκευή cryptodev.
Η χρήση του σημαφόρου συμβάλει στην αποφυγή των busy waits που θα είχαμε αν χρησιμοποιούσαμε spinlocks. 

Συμπληρώθηκε ο κώδικας στο αρχείο `virtio-chrdev.c` των συναρτήσεων `crypto_chrdev_open`, `crypto_chrdev_release` και `crypto_chrdev_ioctl`.
Ουσιαστικά αυτές οι συναρτήσεις πρέπει να στείλουν μήνυμα στον host με όλους τους απαραίτητους buffers από το userspace του guest.
Αυτό επιτυγχάνεται με τη χρήση της διεπαφής των **virtqueues** στον πυρήνα του Linux.

Πιο συγκεκριμένα οι συναρτήσεις αυτές φτιάχνουν μια `scatterlist` για κάθε struct ή μεταβλητή που στέλνουν στον host και τις προσθέτουν στη virtqueue.
Στην περίπτωση της `ioctl` με `copy_from_user` αντιγράφεται ο buffer από το χρήστη με τα δεδομένα. 
Παίρνουν το σημαφόρο και στέλνουν το virtqueue. 
Μετά περιμένουν την απάντηση του host.
Όταν τη λάβουν απελευθερώνουν το σημαφόρο και προωθούν τα αποτελέσματα πίσω στο χρήστη αν υπάρχουν (στην περίπτωση της `ioctl` με `copy_to_user`).
Ο σημαφόρος σε αυτήν την περίπτωση αποτρέπει το ενδεχόμενο δύο διαφορετικά προγράμματα να μεταβάλλουν την virtqueue.

Ένα προφανές πρόβλημα που προκύπτει είναι ότι ο guest πρέπει να κάνει busy waiting μέχρι ο host να απαντήσει.
Χρησιμοποιήσαμε την εντολή `cpu_relax` για την μείωση της χρήσης του επεξεργαστή κατά τη διάρκεια του busy waiting.

Επίσης στην `ioctl` έπρεπε να γίνει αντιγραφή από τον χρήστη του struct με τα δεδομένα.
Όμως υπήρχαν πεδία-δείκτες που στον host δε θα αναγνωρίζονταν γιατί αυτός έχει διαφορετικό address space.
Μέσω της virtqueue τα structs γίνονται map στο χώρο του host αλλά οι εσωτερικοί δείκτες δεν έχουν πλέον νόημα.
Γι'αυτό κάναμε deep copy των structs αντιγράφοντας και περνώντας στον host και τα εσωτερικά πεδία στα οποία αντιστοιχούν οι δείκτες.

## Backend driver

Ο backend driver είναι επέκταση του κώδικα του QEMU και γι'αυτό πρέπει να μεταγλωττιστεί ξανά ολόκληρο το QEMU έτσι ώστε ο paravirtualized driver μας να δουλεύει.

Προσθέσαμε κώδικα στο αρχείο `virtio-cryptodev.c`.
Εδώ απλά κάνουμε pop το πρώτο στοιχείο της virtqueue και ελέγχουμε ποια είναι η κλήση συστήματος. 
Στη συνέχεια την εκτελούμε εκ μέρους του guest και στέλνουμε πίσω το αποτέλεσμα με τη διεπαφή των virtqueues που προσφέρει ο κώδικας του QEMU. 

Ο λόγος που ο driver επιταχύνεται με την χρήση του VirtIO είναι ότι μειώνονται τα copies.
Χωρίς την εικονική συσκευή θα γίνονταν τα διπλάσια copies αλλά τώρα αξιοποιούμε τα virtqueues που μεταφράζουν τις διευθύνσεις μεταξύ των δύο address spaces (του host και του guest).


# Κώδικες

## 1ο και 2ο ζητούμενο

```c
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>
#include <limits.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/select.h>

#include <crypto/cryptodev.h>
#define DATA_SIZE       256
#define BLOCK_SIZE      16
#define KEY_SIZE        16  /* AES128 */

#include <arpa/inet.h>
#include <netinet/in.h>
#define MSG_MAX_SIZE 1024
#define TCP_BACKLOG 5

unsigned char iv[BLOCK_SIZE] = "rmi+HuMPrgj8YDZa";
unsigned char password[KEY_SIZE];

/*
interface:

a.out connect host port [password]
a.out listen port [password] 
*/

unsigned char buf_encr[256], buf_plain[256];
int use_encryption;

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt) {
	ssize_t ret;
	size_t orig_cnt = cnt;

	while (cnt > 0) {
		ret = write(fd, buf, cnt);
		if (ret < 0)
			return ret;
		buf += ret;
		cnt -= ret;
	}

	return orig_cnt;
}

void my_error(const char *s) {
	if(errno != 0)
		perror(s);
	else
		fprintf(stderr,"%s\n",s);
	exit(EXIT_FAILURE);
}

void usage(char **argv) {
	fprintf(stderr,"%s connect host port [password]\n"
	"%s listen port [password]\n",*argv,*argv);
}

long read_long(char *str) {
	char *endptr;
	errno = 0;    /* To distinguish success/failure after call */

	long val = strtol(str, &endptr, 10);

	/* Check for various possible errors */

	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
	|| (errno != 0 && val == 0)) {
		perror("ERROR: strtol");
		exit(EXIT_FAILURE);
	}
	if (endptr == str) {
		fprintf(stderr, "No digits were found\n");
		exit(EXIT_FAILURE);
	}
	return val;
}

struct session_op sess;
struct crypt_op cryp;
int cfd;

int init_crypto () {
	cfd = open("/dev/crypto", O_RDWR);
	if (cfd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}

	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));

	/*
	 * Get crypto session for AES128
	 */
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = password;

	cryp.len = DATA_SIZE;
	cryp.iv = iv;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}
	
	cryp.ses = sess.ses;

	return 0;
}

int encrypt (unsigned char *plaintext, unsigned char *encrypted) {
	cryp.src = plaintext;
	cryp.dst = encrypted;
	cryp.op = COP_ENCRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		return 1;
	}
	return 0;
}

int decrypt (unsigned char *encrypted, unsigned char *plaintext) {
	cryp.src = encrypted;
	cryp.dst = plaintext;
	cryp.op = COP_DECRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		return 1;
	}
	return 0;
}

int sd;
ssize_t n;

void read_and_send()
{
	int size = 0;
	unsigned char* buf = use_encryption ? buf_encr : buf_plain;
	if (ioctl(0, FIONREAD, &size) == 0 && size > 0){
		memset(buf_plain, 0, DATA_SIZE);
		memset(buf_encr, 0, DATA_SIZE);
		while(size > 0 && (n = read(0,buf_plain,sizeof(buf_plain))) > 0){ // should we 0 terminate?????
			if (use_encryption) {
				//size = DATA_SIZE;
				n = DATA_SIZE;
				if (encrypt(buf_plain, buf_encr) > 0) {
					perror("unable to encrypt");
					exit(1);
				}
			}
			if (insist_write(sd, buf, n) != n) {
				perror("write");
				exit(1);
			}
			size -= n;
		}
		if (n < 0) {
			perror("read");
			exit(1);
		}
	}
}

void receive (int newsd) {
	int size = 0;
	unsigned char* buf = use_encryption ? buf_encr : buf_plain; 
	
	if (ioctl(newsd, FIONREAD, &size) == 0 && size > 0){
		memset(buf_plain, 0, DATA_SIZE);
		memset(buf_encr, 0, DATA_SIZE);
		putchar('>');
		fflush(stdout);
		while(size > 0){
			n = read(newsd, buf, sizeof(buf_plain));
			if (use_encryption) {
				//size = DATA_SIZE;
				n = DATA_SIZE;
				if (decrypt(buf_encr, buf_plain) > 0) {
					perror("unable to decrypt");
					exit(1);
				}
			}
				
			if (n <= 0) {
				if (n < 0)
					perror("read from remote peer failed");
				else
					fprintf(stderr, "Peer went away\n");
				break;
			}
			insist_write(1,buf_plain,n);
			size -= n;
		}
		fflush(stdout);
	}	
}


/* Insist until all of the data has been read */
ssize_t insist_read(int fd, void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;

	while (cnt > 0) {
		ret = read(fd, buf, cnt);
		if (ret < 0)
			return ret;
		buf += ret;
		cnt -= ret;
	}

	return orig_cnt;
}

static int fill_urandom_buf(unsigned char *buf, size_t cnt) {
	int crypto_fd;
	int ret = -1;

	crypto_fd = open("/dev/urandom", O_RDONLY);
	if (crypto_fd < 0)
			return crypto_fd;

	ret = insist_read(crypto_fd, buf, cnt);
	close(crypto_fd);

	return ret;
}

void server(int port) {
	char addrstr[INET_ADDRSTRLEN];
	int newsd;
	socklen_t len;
	struct sockaddr_in sa;

	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Bind to a well-known port */
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port); //χαζομαρα interface
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", port);

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) {
		perror("listen");
		exit(1);
	}
	fd_set rfds;
	
	/* Loop forever, accept()ing connections */
	for (;;) {
		fprintf(stderr, "Waiting for an incoming connection...\n");

		/* Accept an incoming connection */
		len = sizeof(struct sockaddr_in);
		if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
			perror("accept");
			exit(1);
		}
		if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
			perror("could not format IP address");
			exit(1);
		}
		fprintf(stderr, "Incoming connection from %s:%d\n",
			addrstr, ntohs(sa.sin_port));
		sd = newsd;
		/* We break out of the loop when the remote peer goes away */
		for (;;) {
			FD_ZERO(&rfds);
			FD_SET(0,&rfds);
			FD_SET(newsd,&rfds);
			int ret = select(newsd+1,&rfds,NULL,NULL,NULL);
			if(FD_ISSET(0,&rfds))
				read_and_send();
			else if(FD_ISSET(newsd,&rfds))
				receive(newsd);
		}
		/* Make sure we don't leak open files */
		if (close(newsd) < 0)
			perror("close");

	}

	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
		perror("ioctl(CIOCFSESSION)");
	}
}

void client (char *hostname, int port) {
	ssize_t n;
	struct hostent *hp;
	struct sockaddr_in sa;

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Look up remote hostname on DNS */
	if ( !(hp = gethostbyname(hostname))) {
		printf("DNS lookup failed for host %s\n", hostname);
		exit(1);
	}

	/* Connect to remote TCP port */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
	fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
	if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		perror("connect");
		exit(1);
	}
	fprintf(stderr, "Connected.\n");
	fd_set rfds;

	for(;;){
		FD_ZERO(&rfds);
		FD_SET(0,&rfds);
		FD_SET(sd,&rfds);
		int ret = select(sd+1,&rfds,NULL,NULL,NULL);
		if(FD_ISSET(0,&rfds))
			read_and_send();
		else if(FD_ISSET(sd,&rfds))
			receive(sd);
		/*
		 * Let the remote know we're not going to write anything else.
		 * Try removing the shutdown() call and see what happens.
		 */
		/*if (shutdown(sd, SHUT_WR) < 0) {
			perror("shutdown");
			exit(1);
		}*/
	}

	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
		perror("ioctl(CIOCFSESSION)");
	}
}

int main(int argc, char **argv)
{
	char msg[MSG_MAX_SIZE];
	char host[256];
	int port;

	if(argc < 3){
		usage(argv);
		exit(EXIT_FAILURE);
	}if(strcmp(argv[1],"connect") == 0){
		if(argc < 4 || argc > 5){
			usage(argv);
			exit(EXIT_FAILURE);
		}
		strncpy(host,argv[2],256);
		host[255] = 0;
		port = read_long(argv[3]);
		if(argc == 5){
			use_encryption = 1;
			strncpy(password, argv[4], KEY_SIZE);
			
			if (init_crypto() > 0) {
				perror("init_crypto");
				exit(1);
			}
		}
		client(host,port);
	}else if(strcmp(argv[1],"listen") == 0){
		if(argc > 4){
			usage(argv);
			exit(EXIT_FAILURE);
		}
		port = read_long(argv[2]);
		if(argc == 4){
			use_encryption = 1;
			strncpy(password, argv[3], KEY_SIZE);
			if (init_crypto() > 0) {
				perror("init crypto");
				exit(1);
			}
		}
		server(port);
	}else{
		usage(argv);
		exit(EXIT_FAILURE);
	}
}
```

## 3o ζητούμενο

_`crypto-chrdev.c`:_

```c
/*
 * crypto-chrdev.c
 *
 * Implementation of character devices
 * for virtio-cryptodev device 
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr>
 *
 */
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/wait.h>
#include <linux/virtio.h>
#include <linux/virtio_config.h>

#include "crypto.h"
#include "crypto-chrdev.h"
#include "debug.h"

#include "cryptodev.h"

/*
 * Global data
 */
struct cdev crypto_chrdev_cdev;

/**
 * Given the minor number of the inode return the crypto device 
 * that owns that number.
 **/
static struct crypto_device *get_crypto_dev_by_minor(unsigned int minor)
{
	struct crypto_device *crdev;
	unsigned long flags;

	debug("Entering");

	spin_lock_irqsave(&crdrvdata.lock, flags);
	list_for_each_entry(crdev, &crdrvdata.devs, list) {
		if (crdev->minor == minor)
			goto out;
	}
	crdev = NULL;

out:
	spin_unlock_irqrestore(&crdrvdata.lock, flags);

	debug("Leaving");
	return crdev;
}

/*************************************
 * Implementation of file operations
 * for the Crypto character device
 *************************************/

static int crypto_chrdev_open(struct inode *inode, struct file *filp)
{
	unsigned long flags;
	int ret = 0;
	unsigned int len;
	struct crypto_open_file *crof;
	struct crypto_device *crdev;
	unsigned int *syscall_type;
	// int *host_fd;

	struct scatterlist out_sg, in_sg, *sgs[2];

	debug("Entering");

	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_OPEN;
	// host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
	// *host_fd = -1;
	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto fail_open;

	/* Associate this open file with the relevant crypto device. */
	crdev = get_crypto_dev_by_minor(iminor(inode));
	if (!crdev) {
		debug("Could not find crypto device with %u minor", 
		      iminor(inode));
		ret = -ENODEV;
		goto fail_open;
	}

	crof = kzalloc(sizeof(*crof), GFP_KERNEL);
	if (!crof) {
		ret = -ENOMEM;
		goto fail_open;
	}
	crof->crdev = crdev;
	crof->host_fd = -1;
	filp->private_data = crof;

	/**
	 * We need two sg lists, one for syscall_type and one to get the 
	 * file descriptor from the host.
	 **/
	/* ?? */

	/* out_sg is used for syscall_type
	 * in_sg is used for the file descriptor from the host
	 */
	sg_init_one(&out_sg, syscall_type, sizeof(*syscall_type));
	sgs[0] = &out_sg;
	sg_init_one(&in_sg, &crof->host_fd, sizeof(crof->host_fd));
	sgs[1] = &in_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	//spin_lock_irqsave(&crdrvdata.lock, flags);
	if (down_interruptible(&crdev->sm) < 0) goto fail_open;

	if((ret = virtqueue_add_sgs(crdev->vq, sgs, 1, 1, &out_sg, GFP_ATOMIC)) < 0)
		goto fail_open;
	virtqueue_kick(crdev->vq);

	/* Wait until host sends a message */
	/* We add a small delay for lower power */
	while (virtqueue_get_buf(crdev->vq, &len) == NULL)
		cpu_relax();
	up(&crdev->sm);
	//spin_unlock_irqrestore(&crdrvdata.lock, flags);

	/* If host failed to open() return -ENODEV. */
	/* ?? */
	if (crof->host_fd < 0) {
		debug("Host failed to open file");
		ret = -ENODEV;
	}
	
fail_open:
	debug("Leaving");
	return ret;
}

static int crypto_chrdev_release(struct inode *inode, struct file *filp)
{
	unsigned long flags;
	int ret = 0;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	unsigned int len;
	unsigned int *syscall_type;

	struct scatterlist *sgs[2],out1_sg,out2_sg;
	
	debug("Entering");
	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_CLOSE;

	/**
	 * Send data to the host.
	 **/
	/* ?? */
	sg_init_one(&out1_sg, syscall_type, sizeof(*syscall_type));
	sg_init_one(&out2_sg, &crof->host_fd, sizeof(crof->host_fd));
	sgs[0] = &out1_sg;
	sgs[1] = &out2_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	if(down_interruptible(&crdev->sm) <0) goto fail_release;
	//spin_lock_irqsave(&crdrvdata.lock, flags);
	if((ret = virtqueue_add_sgs(crdev->vq,sgs, 2, 0, &out1_sg, GFP_ATOMIC)) < 0)
		goto fail_release;
	virtqueue_kick(crdev->vq);

	/* Wait until host sends a message */
	/* We add a small delay for lower power */
	while (virtqueue_get_buf(crdev->vq, &len) == NULL)
		cpu_relax();
	up(&crdev->sm);
	//spin_unlock_irqrestore(&crdrvdata.lock, flags);
	
fail_release:
	kfree(crof);
	debug("Leaving");
	return ret;
}

static long crypto_chrdev_ioctl(struct file *filp, unsigned int cmd, 
                                unsigned long arg)
{
	long ret = 0;
	int err, *host_fd, *host_return_val;
	uint32_t *ses_id;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	struct virtqueue *vq = crdev->vq;
	struct session_op *session_op;
	struct crypt_op *crypt_op;
	struct scatterlist syscall_type_sg, output_msg_sg,input_msg_sg,
	ses_id_sg, session_op_sg, host_return_val_sg,session_key_sg,
	ioctl_cmd_sg, crypt_op_sg,iv_sg, src_sg, dst_sg, host_fd_sg, *sgs[16];
	unsigned int num_out, num_in, len, *ioctl_cmd;
#define MSG_LEN 100
	unsigned char *output_msg, *input_msg;
	unsigned int *syscall_type;
	unsigned char *session_key, *iv, *dst, *src;

	debug("Entering");

	/**
	 * Allocate all data that will be sent to the host.
	 **/
	output_msg = kzalloc(MSG_LEN, GFP_KERNEL);
	input_msg = kzalloc(MSG_LEN, GFP_KERNEL);
	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_IOCTL;

	host_fd = kmalloc(sizeof(*host_fd), GFP_KERNEL);
	*host_fd = crof->host_fd;
	ioctl_cmd = kmalloc(sizeof(*ioctl_cmd), GFP_KERNEL);
	*ioctl_cmd = cmd;

	num_out = 0;
	num_in = 0;

	/**
	 *  These are common to all ioctl commands.
	 **/
	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sgs[num_out++] = &syscall_type_sg;
	sg_init_one(&host_fd_sg, host_fd, sizeof(*host_fd));
	sgs[num_out++] = &host_fd_sg;
	sg_init_one(&ioctl_cmd_sg, ioctl_cmd, sizeof(*ioctl_cmd));
	sgs[num_out++] = &ioctl_cmd_sg;
	// GFP_KERNEL: It is ok to block
	ses_id = kmalloc(sizeof(*ses_id), GFP_KERNEL);
	session_op = kzalloc(sizeof(*session_op), GFP_KERNEL);
	crypt_op = kzalloc(sizeof(*crypt_op), GFP_KERNEL);

	dst = NULL;
	host_return_val = kzalloc(sizeof(*host_return_val), GFP_KERNEL);
	/* ?? */

	/**
	 *  Add all the cmd specific sg lists.
	 **/
	switch (cmd) {
	case CIOCGSESSION:
		debug("CIOCGSESSION");
		memcpy(output_msg, "Hello HOST from ioctl CIOCGSESSION.", 36);
		input_msg[0] = '\0';
		//out
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		// real
		if ((err = copy_from_user (session_op, (void __user *) arg, sizeof(struct session_op))) > 0) {
			ret = -EFAULT;
			goto fail_ioctl;	
		}
		session_key = kmalloc(session_op->keylen, GFP_KERNEL);
		debug("first cfu OK!");
		if ((err = copy_from_user (session_key,  session_op->key, session_op->keylen)) > 0) {
			ret = -EFAULT;
			kfree(session_key);
			goto fail_ioctl;	
		}
		debug("second cfu OK");
		sg_init_one(&session_key_sg, session_key, session_op->keylen);
		sgs[num_out++] = &session_key_sg;
		kfree(session_key);
		debug("OK OK");
		// in
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;
		//real
		sg_init_one(&host_return_val_sg, host_return_val, sizeof(*host_return_val));
		sgs[num_out + num_in++] = &host_return_val_sg;
		sg_init_one(&session_op_sg, session_op, sizeof(*session_op));
		sgs[num_out + num_in++] = &session_op_sg;
		break;

	case CIOCFSESSION:
		debug("CIOCFSESSION");
		memcpy(output_msg, "Hello HOST from ioctl CIOCFSESSION.", 36);
		input_msg[0] = '\0';
		//out
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		//real
		if ((err = copy_from_user (ses_id, (void __user *) arg, sizeof(*ses_id))) > 0) {
			ret = -EFAULT;
			goto fail_ioctl;	
		}

		sg_init_one(&ses_id_sg, ses_id, sizeof(*ses_id));
		sgs[num_out++] = &ses_id_sg;

		//in
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;
		//real
		sg_init_one(&host_return_val_sg, host_return_val, sizeof(*host_return_val));
		sgs[num_out + num_in++] = &host_return_val_sg;
		break;

	case CIOCCRYPT:
		debug("CIOCCRYPT");
		memcpy(output_msg, "Hello HOST from ioctl CIOCCRYPT.", 33);
		input_msg[0] = '\0';
		//out
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		
		if ((err = copy_from_user (crypt_op, (void __user *) arg, sizeof(struct crypt_op))) > 0) {
			ret = -EFAULT;
			debug("EFAULT FROM main crypt");
			goto fail_ioctl;	
		}
		
		/*kfree(iv); kfree(src); kfree(dst);*/
		iv = kmalloc(VIRTIO_CRYPTODEV_BLOCK_SIZE, GFP_KERNEL);
		src = kmalloc(crypt_op->len, GFP_KERNEL);
		dst = kmalloc(crypt_op->len, GFP_KERNEL);
		if ((err = copy_from_user (iv,  crypt_op->iv, VIRTIO_CRYPTODEV_BLOCK_SIZE)) > 0) {
			ret = -EFAULT;
			goto my_fail;	
		}
		if ((err = copy_from_user (src, crypt_op->src, crypt_op->len)) > 0) {
			ret = -EFAULT;
			goto my_fail;	
		}

		sg_init_one(&crypt_op_sg, crypt_op, sizeof(struct crypt_op));
		sgs[num_out++] = &crypt_op_sg;
		sg_init_one(&src_sg, src, crypt_op->len);
		sgs[num_out++] = &src_sg;
		sg_init_one(&iv_sg, iv, VIRTIO_CRYPTODEV_BLOCK_SIZE);
		sgs[num_out++] = &iv_sg;

		//in
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;

		sg_init_one(&host_return_val_sg, host_return_val, sizeof(int));
		sgs[num_out + num_in++] = &host_return_val_sg;
		sg_init_one(&dst_sg, dst, crypt_op->len);
		sgs[num_out + num_in++] = &dst_sg;
		debug("crypt from user OK");
		break;
my_fail:
		debug("MY FAIL");
		kfree(iv);
		kfree(src);
		kfree(dst);
		goto fail_ioctl;

	default:
		debug("Unsupported ioctl command");

		break;
	}

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	/* ?? Lock ?? */

	if (down_interruptible(&crdev->sm) < 0) {
		ret = -ERESTARTSYS;
		goto fail_ioctl;
	}
	if ((ret = virtqueue_add_sgs(vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC)) < 0)
		goto fail_ioctl;
	virtqueue_kick(vq);
	while (virtqueue_get_buf(vq, &len) == NULL)
		cpu_relax();
	up(&crdev->sm);

	debug("We said: '%s'", output_msg);
	debug("Host answered: '%s'", input_msg);

	if (!(*host_return_val)) {
		switch (cmd) {
			case CIOCGSESSION:
				if (copy_to_user ((void __user *) arg, session_op, sizeof(*session_op)) > 0) {
					ret = -EFAULT;
					goto fail_ioctl;
				}
				
				break;
			case CIOCFSESSION:
				if (copy_to_user ((void __user *) arg, ses_id, sizeof(*ses_id)) > 0) {
					ret = -EFAULT;
					goto fail_ioctl;
				}
				
				break;
			case CIOCCRYPT:
				debug("crypt_dst = %p,dest = %p,crypt_op->len = %u",crypt_op->dst,dst,crypt_op->len);
				if (copy_to_user (((struct crypt_op *)arg)->dst, dst, crypt_op->len) > 0) {
					debug("HERE IS THE PROBLEM");
					ret = -EFAULT;
					goto fail_ioctl;
				}
				break;
			default:
				debug("Unsupported ioctl command");

				break;
		}
	} else
		debug ("*host_return_val == 0");
	
fail_ioctl:
	debug("Leaving");
	kfree(output_msg);
	kfree(crypt_op);
	kfree(host_fd);
	kfree(host_return_val);
	kfree(input_msg);
	kfree(ioctl_cmd);
	kfree(ses_id);
	kfree(session_op);
	kfree(syscall_type);
	return ret;
}

static ssize_t crypto_chrdev_read(struct file *filp, char __user *usrbuf, 
                                  size_t cnt, loff_t *f_pos)
{
	debug("Entering");
	debug("Leaving");
	return -EINVAL;
}

static struct file_operations crypto_chrdev_fops = 
{
	.owner          = THIS_MODULE,
	.open           = crypto_chrdev_open,
	.release        = crypto_chrdev_release,
	.read           = crypto_chrdev_read,
	.unlocked_ioctl = crypto_chrdev_ioctl,
};

int crypto_chrdev_init(void)
{
	int ret;
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;
	
	debug("Initializing character device...");
	cdev_init(&crypto_chrdev_cdev, &crypto_chrdev_fops);
	crypto_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	ret = register_chrdev_region(dev_no, crypto_minor_cnt, "crypto_devs");
	if (ret < 0) {
		debug("failed to register region, ret = %d", ret);
		goto out;
	}
	ret = cdev_add(&crypto_chrdev_cdev, dev_no, crypto_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device");
		goto out_with_chrdev_region;
	}

	debug("Completed successfully");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
out:
	return ret;
}

void crypto_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

	debug("entering");
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	cdev_del(&crypto_chrdev_cdev);
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
	debug("leaving");
}
```

_`crypto.h`:_

```c
#ifndef _CRYPTO_H
#define _CRYPTO_H

#define VIRTIO_CRYPTODEV_BLOCK_SIZE    16

#define VIRTIO_CRYPTODEV_SYSCALL_OPEN  0
#define VIRTIO_CRYPTODEV_SYSCALL_CLOSE 1
#define VIRTIO_CRYPTODEV_SYSCALL_IOCTL 2

/* The Virtio ID for virtio crypto ports */
#define VIRTIO_ID_CRYPTODEV            30

/**
 * Global driver data.
 **/
struct crypto_driver_data {
	/* The list of the devices we are handling. */
	struct list_head devs;

	/* The minor number that we give to the next device. */
	unsigned int next_minor;

	spinlock_t lock;
};
extern struct crypto_driver_data crdrvdata;


/**
 * Device info.
 **/
struct crypto_device {
	/* Next crypto device in the list, head is in the crdrvdata struct */
	struct list_head list;

	/* The virtio device we are associated with. */
	struct virtio_device *vdev;

	struct virtqueue *vq;
	/* ?? Lock ?? */
	struct semaphore sm; 
	/* The minor number of the device. */
	unsigned int minor;
};


/**
 *  Crypto open file.
 **/
struct crypto_open_file {
	/* The crypto device this open file is associated with. */
	struct crypto_device *crdev;

	/* The fd that this device has on the Host. */
	int host_fd;
};

#endif

```

_`virtio-cryptodev.c`:_
```c
/*
 * Virtio Cryptodev Device
 *
 * Implementation of virtio-cryptodev qemu backend device.
 *
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr> 
 * Konstantinos Papazafeiropoulos <kpapazaf@cslab.ece.ntua.gr>
 *
 */

#include "qemu/osdep.h"
#include "qemu/iov.h"
#include "hw/qdev.h"
#include "hw/virtio/virtio.h"
#include "standard-headers/linux/virtio_ids.h"
#include "hw/virtio/virtio-cryptodev.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <crypto/cryptodev.h>

static uint64_t get_features(VirtIODevice *vdev, uint64_t features,
                             Error **errp)
{
    DEBUG_IN();
    return features;
}

static void get_config(VirtIODevice *vdev, uint8_t *config_data)
{
    DEBUG_IN();
}

static void set_config(VirtIODevice *vdev, const uint8_t *config_data)
{
    DEBUG_IN();
}

static void set_status(VirtIODevice *vdev, uint8_t status)
{
    DEBUG_IN();
}

static void vser_reset(VirtIODevice *vdev)
{
    DEBUG_IN();
}

static void vq_handle_output(VirtIODevice *vdev, VirtQueue *vq)
{
    VirtQueueElement *elem;
    unsigned int *syscall_type;

    DEBUG_IN();

    elem = virtqueue_pop(vq, sizeof(VirtQueueElement));
    if (!elem) {
        DEBUG("No item to pop from VQ :(");
        return;
    } 

    DEBUG("I have got an item from VQ :)");

    syscall_type = elem->out_sg[0].iov_base;
    switch (*syscall_type) {
    case VIRTIO_CRYPTODEV_SYSCALL_TYPE_OPEN:
        DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_OPEN");
        /* ?? */
        puts("WE CAN PRINT\n");

	*((int *)elem->in_sg[0].iov_base) = open("/dev/crypto", O_RDWR);
	
	printf("open value = %d\n",*((int *)elem->in_sg[0].iov_base));
        break;

    case VIRTIO_CRYPTODEV_SYSCALL_TYPE_CLOSE:
        DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_CLOSE");
        /* ?? */
        close(*((int *)elem->out_sg[1].iov_base));

        break;

    case VIRTIO_CRYPTODEV_SYSCALL_TYPE_IOCTL:
        DEBUG("VIRTIO_CRYPTODEV_SYSCALL_TYPE_IOCTL");
        /* ?? */
        unsigned char *output_msg = elem->out_sg[3].iov_base;
        unsigned char *input_msg = elem->in_sg[0].iov_base;
        memcpy(input_msg, "Host: Welcome to the virtio World!", 35);
        printf("Guest says: %s\n", output_msg);
        printf("We say: %s\n", input_msg);

        int *fd = elem->out_sg[1].iov_base;
        unsigned int *cmd = elem->out_sg[2].iov_base;
        int *ret = elem->in_sg[1].iov_base;

        switch (*cmd) {
            case CIOCGSESSION:
                DEBUG("CIOCGSESSION");
                unsigned char *sess_key = elem->out_sg[4].iov_base;
                struct session_op *sess_op = elem->in_sg[2].iov_base;
                sess_op->key = sess_key;
                *ret = ioctl(*fd, CIOCGSESSION, sess_op);    

                break;
            case CIOCFSESSION:
                // TODO
                DEBUG("CIOCFSESSION");
		uint32_t *session_id = elem->out_sg[4].iov_base;
                *ret = ioctl(*fd, CIOCFSESSION,session_id);
		break;
            case CIOCCRYPT:
                // TODO
                DEBUG("CIOCCRYPT");
		struct crypt_op *c_op;
		c_op = elem->out_sg[4].iov_base;
		c_op->src = elem->out_sg[5].iov_base;
		c_op->iv  = elem->out_sg[6].iov_base;
		c_op->dst = elem->in_sg[2].iov_base;
		*ret = ioctl(*fd, CIOCCRYPT, c_op);
                break;
            default:
                break;
        }
	break;
        default:
            DEBUG("Unknown syscall_type");
            break;
    }

    virtqueue_push(vq, elem, 0);
    virtio_notify(vdev, vq);
    g_free(elem);
}

static void virtio_cryptodev_realize(DeviceState *dev, Error **errp)
{
    VirtIODevice *vdev = VIRTIO_DEVICE(dev);

    DEBUG_IN();

    virtio_init(vdev, "virtio-cryptodev", VIRTIO_ID_CRYPTODEV, 0);
    virtio_add_queue(vdev, 128, vq_handle_output);
}

static void virtio_cryptodev_unrealize(DeviceState *dev, Error **errp)
{
    DEBUG_IN();
}

static Property virtio_cryptodev_properties[] = {
    DEFINE_PROP_END_OF_LIST(),
};

static void virtio_cryptodev_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    VirtioDeviceClass *k = VIRTIO_DEVICE_CLASS(klass);

    DEBUG_IN();
    dc->props = virtio_cryptodev_properties;
    set_bit(DEVICE_CATEGORY_INPUT, dc->categories);

    k->realize = virtio_cryptodev_realize;
    k->unrealize = virtio_cryptodev_unrealize;
    k->get_features = get_features;
    k->get_config = get_config;
    k->set_config = set_config;
    k->set_status = set_status;
    k->reset = vser_reset;
}

static const TypeInfo virtio_cryptodev_info = {
    .name          = TYPE_VIRTIO_CRYPTODEV,
    .parent        = TYPE_VIRTIO_DEVICE,
    .instance_size = sizeof(VirtCryptodev),
    .class_init    = virtio_cryptodev_class_init,
};

static void virtio_cryptodev_register_types(void)
{
    type_register_static(&virtio_cryptodev_info);
}

type_init(virtio_cryptodev_register_types)
```