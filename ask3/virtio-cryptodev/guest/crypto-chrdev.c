/*
 * crypto-chrdev.c
 *
 * Implementation of character devices
 * for virtio-cryptodev device 
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr>
 *
 */
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/wait.h>
#include <linux/virtio.h>
#include <linux/virtio_config.h>

#include "crypto.h"
#include "crypto-chrdev.h"
#include "debug.h"

#include "cryptodev.h"

/*
 * Global data
 */
struct cdev crypto_chrdev_cdev;

/**
 * Given the minor number of the inode return the crypto device 
 * that owns that number.
 **/
static struct crypto_device *get_crypto_dev_by_minor(unsigned int minor)
{
	struct crypto_device *crdev;
	unsigned long flags;

	debug("Entering");

	spin_lock_irqsave(&crdrvdata.lock, flags);
	list_for_each_entry(crdev, &crdrvdata.devs, list) {
		if (crdev->minor == minor)
			goto out;
	}
	crdev = NULL;

out:
	spin_unlock_irqrestore(&crdrvdata.lock, flags);

	debug("Leaving");
	return crdev;
}

/*************************************
 * Implementation of file operations
 * for the Crypto character device
 *************************************/

static int crypto_chrdev_open(struct inode *inode, struct file *filp)
{
	unsigned long flags;
	int ret = 0;
	unsigned int len;
	struct crypto_open_file *crof;
	struct crypto_device *crdev;
	unsigned int *syscall_type;
	// int *host_fd;

	struct scatterlist out_sg, in_sg, *sgs[2];

	debug("Entering");

	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_OPEN;
	// host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
	// *host_fd = -1;
	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto fail_open;

	/* Associate this open file with the relevant crypto device. */
	crdev = get_crypto_dev_by_minor(iminor(inode));
	if (!crdev) {
		debug("Could not find crypto device with %u minor", 
		      iminor(inode));
		ret = -ENODEV;
		goto fail_open;
	}

	crof = kzalloc(sizeof(*crof), GFP_KERNEL);
	if (!crof) {
		ret = -ENOMEM;
		goto fail_open;
	}
	crof->crdev = crdev;
	crof->host_fd = -1;
	filp->private_data = crof;

	/**
	 * We need two sg lists, one for syscall_type and one to get the 
	 * file descriptor from the host.
	 **/
	/* ?? */

	/* out_sg is used for syscall_type
	 * in_sg is used for the file descriptor from the host
	 */
	sg_init_one(&out_sg, syscall_type, sizeof(*syscall_type));
	sgs[0] = &out_sg;
	sg_init_one(&in_sg, &crof->host_fd, sizeof(crof->host_fd));
	sgs[1] = &in_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	//spin_lock_irqsave(&crdrvdata.lock, flags);
	if (down_interruptible(&crdev->sm) < 0) goto fail_open;

	if((ret = virtqueue_add_sgs(crdev->vq, sgs, 1, 1, &out_sg, GFP_ATOMIC)) < 0)
		goto fail_open;
	virtqueue_kick(crdev->vq);

	/* Wait until host sends a message */
	/* We add a small delay for lower power */
	while (virtqueue_get_buf(crdev->vq, &len) == NULL)
		cpu_relax();
	up(&crdev->sm);
	//spin_unlock_irqrestore(&crdrvdata.lock, flags);

	/* If host failed to open() return -ENODEV. */
	/* ?? */
	if (crof->host_fd < 0) {
		debug("Host failed to open file");
		ret = -ENODEV;
	}
	
fail_open:
	debug("Leaving");
	return ret;
}

static int crypto_chrdev_release(struct inode *inode, struct file *filp)
{
	unsigned long flags;
	int ret = 0;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	unsigned int len;
	unsigned int *syscall_type;

	struct scatterlist *sgs[2],out1_sg,out2_sg;
	
	debug("Entering");
	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_CLOSE;

	/**
	 * Send data to the host.
	 **/
	/* ?? */
	sg_init_one(&out1_sg, syscall_type, sizeof(*syscall_type));
	sg_init_one(&out2_sg, &crof->host_fd, sizeof(crof->host_fd));
	sgs[0] = &out1_sg;
	sgs[1] = &out2_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	if(down_interruptible(&crdev->sm) <0) goto fail_release;
	//spin_lock_irqsave(&crdrvdata.lock, flags);
	if((ret = virtqueue_add_sgs(crdev->vq,sgs, 2, 0, &out1_sg, GFP_ATOMIC)) < 0)
		goto fail_release;
	virtqueue_kick(crdev->vq);

	/* Wait until host sends a message */
	/* We add a small delay for lower power */
	while (virtqueue_get_buf(crdev->vq, &len) == NULL)
		cpu_relax();
	up(&crdev->sm);
	//spin_unlock_irqrestore(&crdrvdata.lock, flags);
	
fail_release:
	kfree(crof);
	debug("Leaving");
	return ret;
}

static long crypto_chrdev_ioctl(struct file *filp, unsigned int cmd, 
                                unsigned long arg)
{
	long ret = 0;
	int err, *host_fd, *host_return_val;
	uint32_t *ses_id;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	struct virtqueue *vq = crdev->vq;
	struct session_op *session_op;
	struct crypt_op *crypt_op;
	struct scatterlist syscall_type_sg, output_msg_sg,input_msg_sg,
	ses_id_sg, session_op_sg, host_return_val_sg,session_key_sg,
	ioctl_cmd_sg, crypt_op_sg,iv_sg, src_sg, dst_sg, host_fd_sg, *sgs[16];
	unsigned int num_out, num_in, len, *ioctl_cmd;
#define MSG_LEN 100
	unsigned char *output_msg, *input_msg;
	unsigned int *syscall_type;
	unsigned char *session_key, *iv, *dst, *src;

	debug("Entering");

	/**
	 * Allocate all data that will be sent to the host.
	 **/
	output_msg = kzalloc(MSG_LEN, GFP_KERNEL);
	input_msg = kzalloc(MSG_LEN, GFP_KERNEL);
	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_IOCTL;

	host_fd = kmalloc(sizeof(*host_fd), GFP_KERNEL);
	*host_fd = crof->host_fd;
	ioctl_cmd = kmalloc(sizeof(*ioctl_cmd), GFP_KERNEL);
	*ioctl_cmd = cmd;

	num_out = 0;
	num_in = 0;

	/**
	 *  These are common to all ioctl commands.
	 **/
	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sgs[num_out++] = &syscall_type_sg;
	sg_init_one(&host_fd_sg, host_fd, sizeof(*host_fd));
	sgs[num_out++] = &host_fd_sg;
	sg_init_one(&ioctl_cmd_sg, ioctl_cmd, sizeof(*ioctl_cmd));
	sgs[num_out++] = &ioctl_cmd_sg;
	// GFP_KERNEL: It is ok to block
	ses_id = kmalloc(sizeof(*ses_id), GFP_KERNEL);
	session_op = kzalloc(sizeof(*session_op), GFP_KERNEL);
	crypt_op = kzalloc(sizeof(*crypt_op), GFP_KERNEL);

	dst = NULL;
	host_return_val = kzalloc(sizeof(*host_return_val), GFP_KERNEL);
	/* ?? */

	/**
	 *  Add all the cmd specific sg lists.
	 **/
	switch (cmd) {
	case CIOCGSESSION:
		debug("CIOCGSESSION");
		memcpy(output_msg, "Hello HOST from ioctl CIOCGSESSION.", 36);
		input_msg[0] = '\0';
		//out
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		// real
		if ((err = copy_from_user (session_op, (void __user *) arg, sizeof(struct session_op))) > 0) {
			ret = -EFAULT;
			goto fail_ioctl;	
		}
		session_key = kmalloc(session_op->keylen, GFP_KERNEL);
		debug("first cfu OK!");
		if ((err = copy_from_user (session_key,  session_op->key, session_op->keylen)) > 0) {
			ret = -EFAULT;
			kfree(session_key);
			goto fail_ioctl;	
		}
		debug("second cfu OK");
		sg_init_one(&session_key_sg, session_key, session_op->keylen);
		sgs[num_out++] = &session_key_sg;
		kfree(session_key);
		debug("OK OK");
		// in
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;
		//real
		sg_init_one(&host_return_val_sg, host_return_val, sizeof(*host_return_val));
		sgs[num_out + num_in++] = &host_return_val_sg;
		sg_init_one(&session_op_sg, session_op, sizeof(*session_op));
		sgs[num_out + num_in++] = &session_op_sg;
		break;

	case CIOCFSESSION:
		debug("CIOCFSESSION");
		memcpy(output_msg, "Hello HOST from ioctl CIOCFSESSION.", 36);
		input_msg[0] = '\0';
		//out
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		//real
		if ((err = copy_from_user (ses_id, (void __user *) arg, sizeof(*ses_id))) > 0) {
			ret = -EFAULT;
			goto fail_ioctl;	
		}

		sg_init_one(&ses_id_sg, ses_id, sizeof(*ses_id));
		sgs[num_out++] = &ses_id_sg;

		//in
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;
		//real
		sg_init_one(&host_return_val_sg, host_return_val, sizeof(*host_return_val));
		sgs[num_out + num_in++] = &host_return_val_sg;
		break;

	case CIOCCRYPT:
		debug("CIOCCRYPT");
		memcpy(output_msg, "Hello HOST from ioctl CIOCCRYPT.", 33);
		input_msg[0] = '\0';
		//out
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		
		if ((err = copy_from_user (crypt_op, (void __user *) arg, sizeof(struct crypt_op))) > 0) {
			ret = -EFAULT;
			debug("EFAULT FROM main crypt");
			goto fail_ioctl;	
		}
		
		/*kfree(iv); kfree(src); kfree(dst);*/
		iv = kmalloc(VIRTIO_CRYPTODEV_BLOCK_SIZE, GFP_KERNEL);
		src = kmalloc(crypt_op->len, GFP_KERNEL);
		dst = kmalloc(crypt_op->len, GFP_KERNEL);
		if ((err = copy_from_user (iv,  crypt_op->iv, VIRTIO_CRYPTODEV_BLOCK_SIZE)) > 0) {
			ret = -EFAULT;
			goto my_fail;	
		}
		if ((err = copy_from_user (src, crypt_op->src, crypt_op->len)) > 0) {
			ret = -EFAULT;
			goto my_fail;	
		}

		sg_init_one(&crypt_op_sg, crypt_op, sizeof(struct crypt_op));
		sgs[num_out++] = &crypt_op_sg;
		sg_init_one(&src_sg, src, crypt_op->len);
		sgs[num_out++] = &src_sg;
		sg_init_one(&iv_sg, iv, VIRTIO_CRYPTODEV_BLOCK_SIZE);
		sgs[num_out++] = &iv_sg;

		//in
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;

		sg_init_one(&host_return_val_sg, host_return_val, sizeof(int));
		sgs[num_out + num_in++] = &host_return_val_sg;
		sg_init_one(&dst_sg, dst, crypt_op->len);
		sgs[num_out + num_in++] = &dst_sg;
		debug("crypt from user OK");
		break;
my_fail:
		debug("MY FAIL");
		kfree(iv);
		kfree(src);
		kfree(dst);
		goto fail_ioctl;

	default:
		debug("Unsupported ioctl command");

		break;
	}

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	/* ?? Lock ?? */

	if (down_interruptible(&crdev->sm) < 0) {
		ret = -ERESTARTSYS;
		goto fail_ioctl;
	}
	if ((ret = virtqueue_add_sgs(vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC)) < 0)
		goto fail_ioctl;
	virtqueue_kick(vq);
	while (virtqueue_get_buf(vq, &len) == NULL)
		cpu_relax();
	up(&crdev->sm);

	debug("We said: '%s'", output_msg);
	debug("Host answered: '%s'", input_msg);

	if (!(*host_return_val)) {
		switch (cmd) {
			case CIOCGSESSION:
				if (copy_to_user ((void __user *) arg, session_op, sizeof(*session_op)) > 0) {
					ret = -EFAULT;
					goto fail_ioctl;
				}
				
				break;
			case CIOCFSESSION:
				if (copy_to_user ((void __user *) arg, ses_id, sizeof(*ses_id)) > 0) {
					ret = -EFAULT;
					goto fail_ioctl;
				}
				
				break;
			case CIOCCRYPT:
				debug("crypt_dst = %p,dest = %p,crypt_op->len = %u",crypt_op->dst,dst,crypt_op->len);
				// THIS DOESNT WORK 
				if (copy_to_user (((struct crypt_op *)arg)->dst, dst, crypt_op->len) > 0) {
					debug("HERE IS THE PROBLEM");
					ret = -EFAULT;
					goto fail_ioctl;
				}
			/*kfree(iv);
			kfree(src);
			kfree(dst);*/
				break;
			default:
				debug("Unsupported ioctl command");

				break;
		}
	} else
		debug ("*host_return_val == 0");
	
fail_ioctl:
	debug("Leaving");
	kfree(output_msg);
	kfree(crypt_op);
	kfree(host_fd);
	kfree(host_return_val);
	kfree(input_msg);
	kfree(ioctl_cmd);
	kfree(ses_id);
	kfree(session_op);
	kfree(syscall_type);
	return ret;
}

static ssize_t crypto_chrdev_read(struct file *filp, char __user *usrbuf, 
                                  size_t cnt, loff_t *f_pos)
{
	debug("Entering");
	debug("Leaving");
	return -EINVAL;
}

static struct file_operations crypto_chrdev_fops = 
{
	.owner          = THIS_MODULE,
	.open           = crypto_chrdev_open,
	.release        = crypto_chrdev_release,
	.read           = crypto_chrdev_read,
	.unlocked_ioctl = crypto_chrdev_ioctl,
};

int crypto_chrdev_init(void)
{
	int ret;
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;
	
	debug("Initializing character device...");
	cdev_init(&crypto_chrdev_cdev, &crypto_chrdev_fops);
	crypto_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	ret = register_chrdev_region(dev_no, crypto_minor_cnt, "crypto_devs");
	if (ret < 0) {
		debug("failed to register region, ret = %d", ret);
		goto out;
	}
	ret = cdev_add(&crypto_chrdev_cdev, dev_no, crypto_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device");
		goto out_with_chrdev_region;
	}

	debug("Completed successfully");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
out:
	return ret;
}

void crypto_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

	debug("entering");
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	cdev_del(&crypto_chrdev_cdev);
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
	debug("leaving");
}
