#options add --toc
pandoc $1.md -cstyle.css --highlight-style pygments --standalone  --mathjax -o $1.html && firefox $1.html
