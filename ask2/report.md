---
mainfont: DejaVu Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "Εργαστήριο λειτουργικών συστημάτων, 2η Σειρά ασκήσεων"
author:
- "Αγιάννης Κωνσταντίνος 03116352"
- "Καπελώνης Ελευθέριος 03116163"
---

# Στόχος της άσκησης

Ο στόχος της άσκησης είναι η εξοικείωση με τον προγραμματισμό σε πυρήνα Linux και συγκεκριμένα με τους οδηγούς συσκευών.
Το ζητούμενο ήταν η υλοποίηση του απλού οδηγού LUNIX:TNG για ένα δίκτυο αισθητήρων.
Ο οδηγός πρέπει να λαμβάνει δεδομένα μετρήσεων από αισθητήρες υπό τη μορφή bytes από τον σταθμό βάσης που είναι συνδεδεμένος μέσω USB με τον υπολογιστή, να τα μορφοποιεί και να τα προωθεί στο χώρο χρήστη.
Χρειάζεται να προσφέρει κατάλληλο *μηχανισμό* που επιτρέπει την ταυτόχρονη πρόσβαση στα δεδομένα από πολλές διεργασίες.

# Γενική περιγραφή της αρχιτεκτονικής

Τα δεδομένα φτάνουν μέσα στο υπολογιστικό σύστημα με το πρωτόκολλο USB μέσω του αποίου αποστέλονται δεδομένα σειριακής. Για αυτή τη διασύνδεση το Linux διαθέτει έτοιμο driver.
Στη συνέχεια προωθούνται όχι μέσω της συνηθισμένης διάταξης γραμμής (line discipline) αλλά μέσω της Lunix:TNG line discipline.
Έτσι εμποδίζεται η άμεση ανάγνωσή τους από το χρήστη μέσω ειδικού αρχείου.
Η γραμμή διάταξης τα προωθεί στο στρώμα Lunix protocol που τα μετατρέπει σε πακέτα και χρησιμοποιώντας ένα αυτόματο πεπερασμένων καταστάσεων 
αποθηκεύει τις μετρήσεις σε buffer, ένα για κάθε αισθητήρα (Lunix sensor buffer).
Αυτό είναι το πρώτο μέρος του οδηγού.

Το δεύτερο μέρος του οδηγού συνίσταται στην ανάκτηση των δεδομένων από τους buffers και στην εξαγωγή τους στο χώρο χρήστη. 
Αυτό γίνεται μέσω της υλοποίησης του οδηγού συσκευής χαρακτήρων Lunix.
Σε αυτό το στάδιο παρέχεται η πραγματοποίηση της διεπαφής με το χρήστη μέσω της δημιουργίας ειδικών αρχείων.
Κάθε ζευγάρι αισθητήρα και μετρούμενου μεγέθους αντιστοιχίζεται σε ένα ειδικό αρχείο.
Τελικά η ανάγνωση των δεδομένων από το χρήστη γίνεται μέσω κλήσεων συστήματος και συγκεκριμένα μέσω της `read` σε ένα ήδη ανοιχτό ειδικό αρχείο αφού δηλαδή προηγηθεί η κλήση συστήματος `open`. 

# Τμήμα εξαγωγής δεδομένων στο χώρο του χρήστη

Θα εστιάσουμε στο τμήμα εξαγωγής δεδομένων στο χώρο του χρήστη δηλαδή στην υλοποίηση των system calls που ήταν εκεί που προσθέσαμε τον δικό μας κώδικα.
Συγκεκριμένα δουλέψαμε πάνω στα *lunix_chrdev.c* και *lunix_chrdev.h*.

Για κάθε διεργασία που καλεί την `open` πάνω σε ειδικό αρχείο που χειρίζεται ο οδηγός Lunix δημιουργείται ένας buffer ο οποίος κρατάει τα δεδομένα σε αναγνώσιμη μορφή.
Ακολουθεί εξήγηση για τη λειτουργία των συναρτήσεων που υλοποιήθηκαν.

## Συνάρτηση `lunix_chrdev_state_needs_refresh`

Ελέγχει αν υπάρχουν καινούρια διαθέσιμα δεδομένα από τον αισθητήρα στους Lunix sensor buffers για να αποφασιστεί αν χρειάζεται ενημέρωση οι buffers του `lunix_chrdev_state_struct`.

## Συνάρτηση `lunix_chrdev_state_update`

Ελέγχει με τη βοήθεια της `lunix_chrdev_state_needs_refresh` αν υπάρχουν καινούρια δεδομένα και αν υπάρχουν τα ανακτάει από τον sensor buffer.
Τα παραπάνω γίνονται ενώ κρατάμε το spinlock του sensor ώστε να σταματήσουν να έρχονται νέα δεδομένα από το δίκτυο αισθητήρων.
Αμέσως απελευθερώνουμε το spinlock εφόσον πάρουμε τα δεδομένα.

Στη συνέχεια χρησιμοποιώντας έναν από τους πίνακες `lookup_voltage`, `lookup_temperature` ή `lookup_light` τα δεδομένα μετατρέπονται στην επιθυμητή μορφή.
Με τη χρήση της συνάρτησης `snprintf` μορφοποιούνται ώστε να αποκτήσουν την τελική αναγνώσιμη μορφή και αποθηκεύονται στον buffer του `lunix_chrdev_state_struct`.  

## Συνάρτηση `lunix_chrdev_open`

Αυτή η συνάρτηση καλείται όταν ο χρήστης καλεί την system call `open`.
Εδώ γίνεται η αρχικοποίηση των πεδίων της δομής `lunix_chrdev_state_struct`. 
Αρχικά βρίσκουμε τον minor αριθμό του αρχείου στο οποίο έγινε η `open`.
Με βάση αυτό συμπεραίνουμε τον τύπο του αισθητήρα καθώς και τον αριθμό του.
Αφού αρχικοποιήσουμε το `lunix_chrdev_state_struct` συνδέουμε τον δείκτη `private_data` με αυτή τη δομή.
Υπάρχει ένα `lunix_chrdev_state_struct` ανά ανοιγμένο αρχείο, ενώ υπάρχει ένα `lunix_sensor_struct` ανά αισθητήρα.

## Συνάρτηση `lunix_chrdev_release` 

Εδώ γίνεται η απελευθέρωση των πόρων που έχουν δεσμευτεί.
Καλείται όταν ο χρήστης καλέσει την system call `close`.

## Συνάρτηση `lunix_chrdev_read`

Αυτή είναι η βασική συνάρτηση του οδηγού μας.
Δουλειά της είναι να μεταφέρει τα δεδομένα στο userspace όταν κληθεί η system call `read`.
Κλειδώνει τον σημαφόρο του `lunix_chrdev_state_struct` έτσι ώστε να μην μπορεί άλλη διεργασία ή νήμα να κάνει `read` από το ίδιο ανοιχτό αρχείο (διεργασία-παιδί ή νήμα της ίδιας διεργασίας).
Κάθε ανοιχτό αρχείο έχει το `f_pos` το οποίο δείχνει στην επόμενη θέση στο buffer `buf_data` του `lunix_chrdev_state_struct` που πρόκειται να διαβαστεί. 
Με βάση το `f_pos` λοιπόν και το πόσα bytes ζητήθηκαν μέσω της `read` καθορίζονται τα bytes που θα αντιγραφούν στον buffer του χρήστη.
Ωστόσο αν φτάσουμε στο τέλος του buffer του character device τότε δεν συνεχίζουμε από την αρχή όσα bytes και αν ζητήθηκαν.

Αν το `*fpos` είναι `0` τότε σημαίνει ότι πρέπει να γίνει ενημέρωση από τους buffers των αισθητήρων.
Αν όμως δεν υπάρχουν διαθέσιμα καινούρια δεδομένα τότε η διεργασία πρέπει να κοιμηθεί έως ότου αυτά φτάσουν.
Γι' αυτό το σκοπό χρησιμοποιούμε την `wait_event_interruptible` με συνθήκη αφύπνισης την `lunix_chrdev_state_needs_refresh`.
Όσο η διεργασία κοιμάται ξεκλειδώνουμε τον σημαφόρο.
Αν έχει οριστεί η σημαία `O_NONBLOCK` μέσω της `open` τότε η διεργασία έχει ζητήσει να μην μπλοκάρεται η εκτέλεσή της οπότε η `read` αντί να την προσθέσει στην ουρά επιστρέφει.

Η αντιγραφή των δεδομένων στον buffer του χρήστη γίνεται απλά με τη χρήση της `copy_to_user` η οποία ελέγχει και αν ο δείκτης στον buffer αντιστοιχεί σε μνήμη που ανήκει στην διεργασία.
Αν χρειαστεί γίνεται επαναφορά του `*f_pos` στο `0` και επιστρέφεται ο αριθμός των bytes που αντιγράφτηκαν.

## Συνάρτηση `lunix_chrdev_init` 

Εδώ γίνεται η εγγραφή του character device στον πυρήνα.

Με την `cdev_init` ορίζουμε το struct `file_operations` που έχει τους δείκτες στις συναρτήσεις `lunix_chrdev_open`, `lunix_chrdev_release`, `lunix_chrdev_read`, `lunix_chrdev_ioctl` και `lunix_chrdev_mmap`.

Με την `register_chrdev_region` ορίζουμε τον major number του οδηγού και τον αριθμό των minor numbers.
Χρησιμοποιούνται 8 minor numbers για κάθε αισθητήρα.
Συνολικά ο αριθμός των αισθητήρων μπορεί να φτάνει τους 16.

Τελικά με την `cdev_add` προσθέτουμε τον character device μας.

## Συνάρτηση `lunix_chrdev_destroy` 

Εδώ γίνεται διαγραφή του character device και αποδέσμευση των major και minor numbers του.

# Ολοκληρωμένος κώδικας του αρχείου `lunix-chrdev.c`

```c
/*
 * Kernel API https://www.kernel.org/doc/html/v4.12/index.html
 * tutorial https://linux-kernel-labs.github.io/master/labs/device_drivers.html
 * link mentioning how to create /dev/lunix* from inside the kernel
 * http://olegkutkov.me/2018/03/14/simple-linux-character-device-driver/
 */

/*
 * lunix-chrdev.c
 *
 * Implementation of character devices
 * for Lunix:TNG
 *
 * < Your name here >
 *
 */

#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/mmzone.h>
#include <linux/vmalloc.h>
#include <linux/spinlock.h>

#include "lunix.h"
#include "lunix-chrdev.h"
#include "lunix-lookup.h"

/*
 * Global data
 */
struct cdev lunix_chrdev_cdev;

/*
 * Just a quick [unlocked] check to see if the cached
 * chrdev state needs to be updated from sensor measurements.
 */
static inline int lunix_chrdev_state_needs_refresh(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor;
	
	/* Make assignment and check for NULL pointer */
	WARN_ON ( !(sensor = state->sensor));

	return state->buf_timestamp != sensor->msr_data[state->type]->last_update;
	//return 0;
}

/*
 * Updates the cached state of a character device
 * based on sensor data. Must be called with the
 * character device state lock held.
 */
static int lunix_chrdev_state_update(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor = state->sensor;
	int temp, ret;
	//debug("entering lunix state update");

	/*
	 * Grab the raw data quickly, hold the
	 * spinlock for as little as possible.
	 */

	spin_lock(&sensor->lock);
	/* Why use spinlocks? See LDD3, p. 119 */

	/*
	 * Any new data available?
	 */
	int update_needed = lunix_chrdev_state_needs_refresh(state);
	uint32_t value = sensor->msr_data[state->type]->values[0];
	uint32_t timestamp = sensor->msr_data[state->type]->last_update;
	spin_unlock(&sensor->lock);

	/* it would be better if they created a 2 dimensional array */
	if(update_needed){
		switch(state->type){
			case BATT:
				temp = lookup_voltage[value];
				break;
			case TEMP:
				temp = lookup_temperature[value];
				break;
			case LIGHT:
				temp = lookup_light[value];
		}
	}
	/*
	 * Now we can take our time to format them,
	 * holding only the private state semaphore
	 */

	if(update_needed) {
		debug("updating the buffer data\n");
		snprintf(state->buf_data,LUNIX_CHRDEV_BUFSZ,"%d.%d\n",temp/1000,abs(temp)%1000);
		state->buf_timestamp = timestamp;
	}

	//debug("leaving lunix state update\n");
	//debug("update_needed=%d", update_needed);
	ret = (update_needed ? 0 : -EAGAIN /* no new data */ );
out:
	return ret;
}

/*************************************
 * Implementation of file operations
 * for the Lunix character device
 *************************************/

static int lunix_chrdev_open(struct inode *inode, struct file *filp)
{
	/* Declarations */
	unsigned int minor_num;
	struct lunix_chrdev_state_struct *lunix_chrdev_state;
	int ret;

	debug("entering\n");
	ret = -ENODEV; /* No such device */
	/* make the file non seekable */
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto out;

	/*
	 * Associate this open file with the relevant sensor based on
	 * the minor number of the device node [/dev/sensor<NO>-<TYPE>]
	 */
	minor_num = iminor(inode);
	debug("open file associated with minor number = %u\n", minor_num);
	
	/* Allocate a new Lunix character device private state structure */
	lunix_chrdev_state = kzalloc(sizeof(struct lunix_chrdev_state_struct), GFP_KERNEL);

	/* Get 3 last bits of minor number */
	lunix_chrdev_state->type = minor_num & 7;
	/* Find the sensor */
	lunix_chrdev_state->sensor = &lunix_sensors[(minor_num >> 3)];

	/* init semaphore */
	sema_init(&lunix_chrdev_state->lock,1);

	debug("type = %usensor = %u\n", lunix_chrdev_state->type, minor_num>>3);
	filp->private_data = (struct lunix_chrdev_state_struct *) lunix_chrdev_state;

out:
	debug("leaving, with ret = %d\n", ret);
	return ret;
}

static int lunix_chrdev_release(struct inode *inode, struct file *filp)
{
	kzfree(filp->private_data);
	debug("releasing resources\n");
	return 0;
}

static long lunix_chrdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Why? */
	return -EINVAL; /* invalid argument */
}

static ssize_t lunix_chrdev_read(struct file *filp, char __user *usrbuf, size_t cnt, loff_t *f_pos)
{
	debug("entering read\n");
	ssize_t ret;
	struct lunix_sensor_struct *sensor;
	struct lunix_chrdev_state_struct *state;

	state = filp->private_data;
	WARN_ON(!state);

	sensor = state->sensor;
	WARN_ON(!sensor);

	/* semaphore lock */
	if(down_interruptible(&state->lock)) {
		printk(KERN_INFO " could not hold semaphore");
		return -1;
	}
	/*
	 * If the cached character device state needs to be
	 * updated by actual sensor data (i.e. we need to report
	 * on a "fresh" measurement, do so
	 */

	if (*f_pos == 0) {
		while (lunix_chrdev_state_update(state) == -EAGAIN) {
			/* No data available right now, try again later */
			/* Release the lock so that children can acquire it */
			up(&state->lock);
			if (filp->f_flags & O_NONBLOCK) {
				ret = -EAGAIN;
				goto out;
			}

			/* The process needs to sleep */
			if (wait_event_interruptible(state->sensor->wq,lunix_chrdev_state_needs_refresh(state)))
				return -ERESTARTSYS;
			/* Reacquire the lock */
			if(down_interruptible(&state->lock)) {
				printk(KERN_INFO " could not hold semaphore");
				return -1;
			}
			//lunix_chrdev_state_update(state);
		}
	}


	/* Determine the number of cached bytes to copy to userspace */
	size_t len = strlen(state->buf_data);
	/* 0 is exluded from len by strlen */
	size_t copy_len = min(cnt,(size_t)(len - *f_pos));

	if(copy_to_user(usrbuf,state->buf_data + *f_pos,copy_len)){
		ret = -EFAULT;
		goto out;
	}
	*f_pos += copy_len;
	/* if we have reached the end */
	if(*f_pos == len)
		*f_pos = 0;
	ret = copy_len;
out:
	/* semaphore unlock */
	up(&state->lock);
	debug("leaving read\n");
	return ret;
}

static int lunix_chrdev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	return -EINVAL; /* invalid argumet */
}

static struct file_operations lunix_chrdev_fops = 
{
	.owner          = THIS_MODULE,
	.open           = lunix_chrdev_open,
	.release        = lunix_chrdev_release,
	.read           = lunix_chrdev_read,
	.unlocked_ioctl = lunix_chrdev_ioctl,
	.mmap           = lunix_chrdev_mmap
};

int lunix_chrdev_init(void)
{
	/*
	 * Register the character device with the kernel, asking for
	 * a range of minor numbers (number of sensors * 8 measurements / sensor)
	 * beginning with LINUX_CHRDEV_MAJOR:0
	 */
	int ret;
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;
	
	debug("initializing character device\n");
	cdev_init(&lunix_chrdev_cdev, &lunix_chrdev_fops);
	lunix_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
	/* register character device */
	/* https://www.fsl.cs.sunysb.edu/kernel-api/re939.html */
	ret = register_chrdev_region(dev_no,lunix_minor_cnt,"LUNIX character device. OSlab");
	if (ret < 0) {
		debug("failed to register region, ret = %d\n", ret);
		goto out;
	}	
	/* add character device */
	/* https://www.fsl.cs.sunysb.edu/kernel-api/re943.html */
	ret = cdev_add(&lunix_chrdev_cdev,dev_no,lunix_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device\n");
		goto out_with_chrdev_region;
	}
	debug("completed successfully\n");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
out:
	return ret;
}

void lunix_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;

	debug("entering\n");
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
	cdev_del(&lunix_chrdev_cdev);
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
	debug("leaving\n");
}
```

