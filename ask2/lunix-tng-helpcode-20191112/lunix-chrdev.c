/*
 * Kernel API https://www.kernel.org/doc/html/v4.12/index.html
 * tutorial https://linux-kernel-labs.github.io/master/labs/device_drivers.html
 * link mentioning how to create /dev/lunix* from inside the kernel
 * http://olegkutkov.me/2018/03/14/simple-linux-character-device-driver/
 */

/*
 * lunix-chrdev.c
 *
 * Implementation of character devices
 * for Lunix:TNG
 *
 * < Your name here >
 *
 */

#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/mmzone.h>
#include <linux/vmalloc.h>
#include <linux/spinlock.h>

#include "lunix.h"
#include "lunix-chrdev.h"
#include "lunix-lookup.h"

/*
 * Global data
 */
struct cdev lunix_chrdev_cdev;

/*
 * Just a quick [unlocked] check to see if the cached
 * chrdev state needs to be updated from sensor measurements.
 */
static inline int lunix_chrdev_state_needs_refresh(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor;
	
	/* Make assignment and check for NULL pointer */
	WARN_ON ( !(sensor = state->sensor));

	return state->buf_timestamp != sensor->msr_data[state->type]->last_update;
	//return 0;
}

/*
 * Updates the cached state of a character device
 * based on sensor data. Must be called with the
 * character device state lock held.
 */
static int lunix_chrdev_state_update(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor = state->sensor;
	int temp, ret;
	//debug("entering lunix state update");

	/*
	 * Grab the raw data quickly, hold the
	 * spinlock for as little as possible.
	 */

	spin_lock(&sensor->lock);
	/* Why use spinlocks? See LDD3, p. 119 */

	/*
	 * Any new data available?
	 */
	int update_needed = lunix_chrdev_state_needs_refresh(state);
	uint32_t value = sensor->msr_data[state->type]->values[0];
	uint32_t timestamp = sensor->msr_data[state->type]->last_update;
	spin_unlock(&sensor->lock);

	/* it would be better if they created a 2 dimensional array */
	if(update_needed){
		switch(state->type){
			case BATT:
				temp = lookup_voltage[value];
				break;
			case TEMP:
				temp = lookup_temperature[value];
				break;
			case LIGHT:
				temp = lookup_light[value];
		}
	}
	/*
	 * Now we can take our time to format them,
	 * holding only the private state semaphore
	 */

	if(update_needed) {
		debug("updating the buffer data\n");
		snprintf(state->buf_data,LUNIX_CHRDEV_BUFSZ,"%d.%d\n",temp/1000,abs(temp)%1000);
		state->buf_timestamp = timestamp;
	}

	//debug("leaving lunix state update\n");
	//debug("update_needed=%d", update_needed);
	ret = (update_needed ? 0 : -EAGAIN /* no new data */ );
out:
	return ret;
}

/*************************************
 * Implementation of file operations
 * for the Lunix character device
 *************************************/

static int lunix_chrdev_open(struct inode *inode, struct file *filp)
{
	/* Declarations */
	unsigned int minor_num;
	struct lunix_chrdev_state_struct *lunix_chrdev_state;
	int ret;

	debug("entering\n");
	ret = -ENODEV; /* No such device */
	/* make the file non seekable */
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto out;

	/*
	 * Associate this open file with the relevant sensor based on
	 * the minor number of the device node [/dev/sensor<NO>-<TYPE>]
	 */
	minor_num = iminor(inode);
	debug("open file associated with minor number = %u\n", minor_num);
	
	/* Allocate a new Lunix character device private state structure */
	lunix_chrdev_state = kzalloc(sizeof(struct lunix_chrdev_state_struct), GFP_KERNEL);

	/* Get 3 last bits of minor number */
	lunix_chrdev_state->type = minor_num & 7;
	/* Find the sensor */
	lunix_chrdev_state->sensor = &lunix_sensors[(minor_num >> 3)];

	/* init semaphore */
	sema_init(&lunix_chrdev_state->lock,1);

	debug("type = %usensor = %u\n", lunix_chrdev_state->type, minor_num>>3);
	filp->private_data = (struct lunix_chrdev_state_struct *) lunix_chrdev_state;

out:
	debug("leaving, with ret = %d\n", ret);
	return ret;
}

static int lunix_chrdev_release(struct inode *inode, struct file *filp)
{
	kzfree(filp->private_data);
	debug("releasing resources\n");
	return 0;
}

static long lunix_chrdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Why? */
	return -EINVAL; /* invalid argument */
}

static ssize_t lunix_chrdev_read(struct file *filp, char __user *usrbuf, size_t cnt, loff_t *f_pos)
{
	debug("entering read\n");
	ssize_t ret;
	struct lunix_sensor_struct *sensor;
	struct lunix_chrdev_state_struct *state;

	state = filp->private_data;
	WARN_ON(!state);

	sensor = state->sensor;
	WARN_ON(!sensor);

	/* semaphore lock */
	if(down_interruptible(&state->lock)) {
		printk(KERN_INFO " could not hold semaphore");
		return -1;
	}
	/*
	 * If the cached character device state needs to be
	 * updated by actual sensor data (i.e. we need to report
	 * on a "fresh" measurement, do so
	 */

	if (*f_pos == 0) {
		while (lunix_chrdev_state_update(state) == -EAGAIN) {
			/* No data available right now, try again later */
			/* Release the lock so that children can acquire it */
			up(&state->lock);
			if (filp->f_flags & O_NONBLOCK) {
				ret = -EAGAIN;
				goto out;
			}

			/* The process needs to sleep */
			if (wait_event_interruptible(state->sensor->wq,lunix_chrdev_state_needs_refresh(state)))
				return -ERESTARTSYS;
			/* Reacquire the lock */
			if(down_interruptible(&state->lock)) {
				printk(KERN_INFO " could not hold semaphore");
				return -1;
			}
			//lunix_chrdev_state_update(state);
		}
	}


	/* Determine the number of cached bytes to copy to userspace */
	size_t len = strlen(state->buf_data);
	/* 0 is exluded from len by strlen */
	size_t copy_len = min(cnt,(size_t)(len - *f_pos));

	if(copy_to_user(usrbuf,state->buf_data + *f_pos,copy_len)){
		ret = -EFAULT;
		goto out;
	}
	*f_pos += copy_len;
	/* if we have reached the end */
	if(*f_pos == len)
		*f_pos = 0;
	ret = copy_len;
out:
	/* semaphore unlock */
	up(&state->lock);
	debug("leaving read\n");
	return ret;
}

static int lunix_chrdev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	return -EINVAL; /* invalid argumet */
}

static struct file_operations lunix_chrdev_fops = 
{
	.owner          = THIS_MODULE,
	.open           = lunix_chrdev_open,
	.release        = lunix_chrdev_release,
	.read           = lunix_chrdev_read,
	.unlocked_ioctl = lunix_chrdev_ioctl,
	.mmap           = lunix_chrdev_mmap
};

int lunix_chrdev_init(void)
{
	/*
	 * Register the character device with the kernel, asking for
	 * a range of minor numbers (number of sensors * 8 measurements / sensor)
	 * beginning with LINUX_CHRDEV_MAJOR:0
	 */
	int ret;
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;
	
	debug("initializing character device\n");
	cdev_init(&lunix_chrdev_cdev, &lunix_chrdev_fops);
	lunix_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
	/* register character device */
	/* https://www.fsl.cs.sunysb.edu/kernel-api/re939.html */
	ret = register_chrdev_region(dev_no,lunix_minor_cnt,"LUNIX character device. OSlab");
	if (ret < 0) {
		debug("failed to register region, ret = %d\n", ret);
		goto out;
	}	
	/* add character device */
	/* https://www.fsl.cs.sunysb.edu/kernel-api/re943.html */
	ret = cdev_add(&lunix_chrdev_cdev,dev_no,lunix_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device\n");
		goto out_with_chrdev_region;
	}
	debug("completed successfully\n");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
out:
	return ret;
}

void lunix_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;

	debug("entering\n");
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
	cdev_del(&lunix_chrdev_cdev);
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
	debug("leaving\n");
}
