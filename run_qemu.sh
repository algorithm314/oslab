#!/bin/sh

file=private.qcow2
SSH_PORT=22223

qemu-system-x86_64 -enable-kvm -M pc-0.12 -smp 2 -drive file=$file,if=virtio \
	-m 1024 -chardev socket,id=sensors0,host=cerberus.cslab.ece.ntua.gr,port=49152,ipv4 \
	-net user,hostfwd=tcp::$SSH_PORT-:22 \
	-device isa-serial,chardev=sensors0,id=serial0 -parallel none \
	-net nic,model=virtio -nographic -monitor /dev/null
