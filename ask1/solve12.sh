#!/bin/sh

for file in /tmp/riddle-*
do printf $1 | dd of=$file bs=1 seek=111 count=1 conv=notrunc
done
