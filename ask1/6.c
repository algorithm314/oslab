#include <stdio.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int main(){
	int pipefd[2][2];
	pipe(pipefd[0]);
	pipe(pipefd[1]);
	dup2(pipefd[0][0], 33);
	dup2(pipefd[0][1], 34);
	dup2(pipefd[1][0], 53);
	dup2(pipefd[1][1], 54);
	//write(fd,"\0\0\0\0",4);
	//read(fd2,"\0\0\0\0",4);
	execve("./riddle", NULL, NULL);
}
