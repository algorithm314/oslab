#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <signal.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char **argv){
	int pid = 0;
	while (1) {
		int pid = fork();
		if (pid == -1) {
			perror("error in fork");
		} else if (pid == 0) {
			if (getpid() == 32767) {
				printf("Success\n");
				execve("riddle", NULL, NULL);
			}
			exit(0);
		} else if (pid == 32767) {
			wait(NULL);
			exit(0);
		} else
			wait(NULL);
	}
}
