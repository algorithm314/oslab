#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>
#include <sys/types.h>

int main(){
	int fd = open("/tmp/riddle-erqjES", O_RDWR);
	lseek(fd, SEEK_SET, 111);
	char c = 'V';
	write(fd, &c, 1);
	close(fd);
}
